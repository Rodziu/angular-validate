/*
 * AngularJS validation plugin for Twitter Bootstrap
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const app = angular.module('validate', []);
	/**
	 * Configuration - default validate error texts
	 */
	app.provider('Validate', function(){
		this.validateTexts = {
			required: 'To pole jest wymagane!',
			maxlength: 'Maksymalna długość tego pola to %s znaków!',
			min: 'Minimalna wartość to %s!',
			max: 'Maksymalna wartość to %s!',
			pattern: 'Wartość musi spełniać następujące wyrażenie regularne: %s!',
			number: 'Wartość musi być liczbą!',
			date: 'Podaj poprawną datę!',
			email: 'Podaj poprawny adres e-mail!',
			minlength: 'Minimalna długość tego pola to %s znaków!',
			validateEqual: 'Pola muszą być jednakowe!',
			validateUrl: {
				input: 'Podaj poprawny adres URL (http://domena.pl)',
				textarea: 'Podaj poprawne adresy URL (http://domena.pl), każdy od nowej linii!'
			},
			validateHost: {
				input: 'Podaj poprawny host (domena.pl)!',
				textarea: 'Podaj poprawne adresy host (domena.pl), każdy od nowej linii!'
			}
		};
		// noinspection JSUnusedGlobalSymbols
		this.$get = function(){
			return this.validateTexts;
		};
	});
	/**
	 * Factory that allows one to trigger form validation from JS or reset it's validation state.
	 */
	app.factory('Validator', ['$timeout', function($timeout){
		const validators = {};
		return {
			/**
			 * Register new validate form
			 * @param name
			 * @param form
			 * @param ctrl
			 */
			register: function(name, form, ctrl){
				validators[name] = {
					ctrl: ctrl,
					form: form
				};
			},
			/**
			 * Destroy validation object
			 * @param name
			 */
			unRegister: function(name){
				if(typeof validators[name] === 'undefined'){
					throw new Error('Form `' + name + '` was never defined for validation!');
				}
				delete validators[name];
			},
			/**
			 * Trigger validation
			 * @param name
			 * @returns {boolean|*}
			 */
			validateForm: function(name){
				if(typeof validators[name] === 'undefined'){
					throw new Error('Form `' + name + '` was never defined for validation!');
				}
				validators[name].ctrl.validate();
				return !validators[name].form.$invalid;
			},
			/**
			 * Reset validation state
			 * @param name
			 */
			resetValidator: function(name){
				if(typeof validators[name] === 'undefined'){
					throw new Error('Form `' + name + '` was never defined for validation!');
				}
				$timeout(function(){
					validators[name].ctrl.reset();
				});
			},
			/**
			 * Check if given form is dirty
			 * @param name
			 * @returns {boolean}
			 */
			isDirtyForm: function(name){
				if(typeof validators[name] === 'undefined'){
					throw new Error('Form `' + name + '` was never defined for validation!');
				}
				return validators[name].ctrl.isDirty();
			}
		}
	}]);
	/**
	 * Form
	 */
	app.directive('validate', ['Validator', function(Validator){
		return {
			restrict: 'A',
			scope: true,
			controller: ['$scope', function($scope){
				this.validate = function(){
					$scope.$broadcast("validate");
				};
				this.reset = function(){
					$scope.$broadcast("validatorReset");
					this.setPristine();
				};
			}],
			require: ['form', 'validate'],
			link(scope, element, attr, ctrl){
				const validator = attr['validate'] ? attr['validate'] : (attr.name ? attr.name : '');
				if(validator){
					ctrl[1].isDirty = function(){
						return ctrl[0].$dirty;
					};
					ctrl[1].setPristine = function(){
						ctrl[0].$setPristine();
					};
					Validator.register(validator, ctrl[0], ctrl[1]);
					scope.$on("$destroy", function(){
						Validator.unRegister(validator);
					});
				}
			}
		};
	}]);
	/**
	 * help-block element
	 */
	app.directive('formGroupValidate', [function(){
		return {
			restrict: 'E',
			replace: true,
			require: '^formGroup',
			template: '<span class="help-block" ng-show="errors.length"><span ng-repeat="e in errors" ng-class="{\'text-success\': e.validity, \'text-danger\': !e.validity}" style="display:block;">{{::e.text}}</span></span>',
			scope: true,
			link(scope, element, attr, formGroup){
				scope.errors = formGroup.errors;
			}
		}
	}]);
	/**
	 * form-group - append help-block
	 */
	app.directive('formGroup', ['$compile', function($compile){
		return {
			restrict: 'C',
			require: ['?^validate', 'formGroup'],
			controller: ['$element', function($element){
				const elements = [];
				let id = 0;
				this.errors = [];
				this.invalid = false;
				this.registerElement = function(){
					id++;
					elements.push(id);
					return id;
				};
				this.destroyElement = function(key){
					elements.splice(elements.indexOf(key), 1);
				};
				this.amIFirst = function(key){
					return elements[0] === key;
				};
				this.updateErrors = function(errors, isInvalid){
					angular.copy(errors, this.errors);
					this.invalid = isInvalid;
					if(isInvalid){
						$element.addClass('has-error');
					}else{
						$element.removeClass('has-error');
					}
				};
			}],
			compile(){
				return {
					pre(scope, element, attr, ctrl){
						if(!ctrl[0]){
							return;
						}
						const hb = angular.element('<form-group-validate></form-group-validate>');
						element.append(hb);
						$compile(hb)(scope);
						scope.$on('validatorReset', function(){
							ctrl[1].updateErrors([], false);
						});
					}
				};
			}
		};
	}]);
	/**
	 * Update errors for input/textarea/select elements
	 * @type {*[]}
	 */
	app.directive('formControl', ['Validate', function(Validate){
		return {
			restrict: 'C',
			require: ['?ngModel', '?^formGroup'],
			compile(){
				return {
					pre(scope, element, attr, ctrl){
						for(let c = 0; c < ctrl.length; c++){
							if(!ctrl[c]){
								return;
							}
						}
						/**
						 * Get an array of errors for current field and pass them to formGroup.
						 */
						const id = ctrl[1].registerElement(),
							getErrors = function(){
								if(!ctrl[1].amIFirst(id)){
									return;
								}
								const errors = [];
								let invalid = false;
								for(let e in ctrl[0].$error){
									if(ctrl[0].$error.hasOwnProperty(e)){
										if(ctrl[0].$error[e]){
											invalid = true;
										}
										/**
										 * alternative message from attr
										 */
										const alternate = attr.$normalize(e + '-msg');
										if(
											typeof Validate[e] !== 'undefined'
											|| typeof attr[alternate] !== 'undefined'
										){
											const error = {
												text: Validate[e],
												validity: !ctrl[0].$error[e]
											};
											if(typeof attr[alternate] !== 'undefined'){
												error.text = attr[alternate];
											}else{
												if(typeof error.text === 'object'){
													error.text = element[0].tagName === 'INPUT'
														? error.text.input : error.text.textarea;
												}
												error.text = error.text.replace(
													'%s', e === 'pattern' ? attr['ngPattern'] : attr[e]
												);
											}
											errors.push(error);
										}
									}
								}
								ctrl[1].updateErrors(errors, invalid);
							};
						// disable default error messages, show errors on submit
						element[0].addEventListener('invalid', function(e){
							e.preventDefault();
							getErrors();
							scope.$apply();
							return false;
						});
						//
						scope.$watch(function(){
							return [ctrl[0].$error, ctrl[0].$viewValue];
						}, function(nV, oV){
							if(!angular.equals(nV, oV)){
								getErrors();
							}else{
								ctrl[1].updateErrors([], false);
							}
						}, true);
						//
						scope.$on('validate', function(){
							getErrors();
						});
						scope.$on('$destroy', function(){
							ctrl[1].destroyElement(id);
						});
					}
				};
			}
		};
	}]);
	/**
	 * custom validators
	 */
	/**
	 * Validate Url
	 */
	app.directive('validateUrl', function(){
		const validUrl = function(string){
			return /^https?:\/\/.+/.test(string);
		};
		return {
			restrict: 'A',
			require: 'ngModel',
			link(scope, element, attrs, ctrl){
				const validator = function(viewValue){
					if(typeof viewValue !== 'undefined' && viewValue !== ''){
						if(element[0].tagName === 'TEXTAREA'){
							const rows = viewValue.split(/\r\n|\r|\n/);
							for(let r = 0; r < rows.length; r++){
								const line = rows[r].trim();
								if(line !== ''){
									if(!validUrl(line)){
										ctrl.$setValidity('validateUrl', false);
										element[0].setCustomValidity(" ");
										return undefined;
									}
								}
							}
						}else if(!validUrl(viewValue)){
							ctrl.$setValidity('validateUrl', false);
							element[0].setCustomValidity(" ");
							return undefined;
						}
					}
					ctrl.$setValidity('validateUrl', true);
					element[0].setCustomValidity("");
					return viewValue;
				};
				ctrl.$parsers.unshift(validator);
				ctrl.$formatters.unshift(validator);
			}
		};
	});
	/**
	 * Validate Host
	 */
	app.directive('validateHost', function(){
		const validHost = function(string){
			return /^([a-z\u00A1-\uFFFF0-9]|[a-z\u00A1-\uFFFF0-9][a-z\u00A1-\uFFFF0-9\-]{0,61}[a-z\u00A1-\uFFFF0-9])(\.([a-z\u00A1-\uFFFF0-9]|[a-z\u00A1-\uFFFF0-9][a-z\u00A1-\uFFFF0-9\-]{0,61}[a-z\u00A1-\uFFFF0-9]))*$/i.test(string);
		};
		return {
			restrict: 'A',
			require: 'ngModel',
			link(scope, element, attrs, ctrl){
				const validator = function(viewValue){
					if(typeof viewValue !== 'undefined' && viewValue !== ''){
						if(element[0].tagName === 'TEXTAREA'){
							const rows = viewValue.split(/\r\n|\r|\n/);
							for(let r = 0; r < rows.length; r++){
								const line = rows[r].trim();
								if(line !== ''){
									if(!validHost(line)){
										ctrl.$setValidity('validateHost', false);
										element[0].setCustomValidity(" ");
										return undefined;
									}
								}
							}
						}else if(!validHost(viewValue)){
							ctrl.$setValidity('validateHost', false);
							element[0].setCustomValidity(" ");
							return undefined;
						}
					}
					ctrl.$setValidity('validateHost', true);
					element[0].setCustomValidity("");
					return viewValue;
				};
				ctrl.$parsers.unshift(validator);
				ctrl.$formatters.unshift(validator);
			}
		};
	});
	/**
	 * minlength
	 */
	app.directive('minlength', function(){
		return {
			restrict: 'A',
			require: 'ngModel',
			link(scope, element, attrs, ctrl){
				const validator = function(viewValue){
					const min = parseInt(attrs['validateMinlength']),
						length = typeof viewValue !== 'string' ? 0 : viewValue.length;
					if(!isNaN(min) && min > length){
						ctrl.$setValidity('validateMinlength', false);
						element[0].setCustomValidity(" ");
						return undefined;
					}
					ctrl.$setValidity('validateMinlength', true);
					element[0].setCustomValidity("");
					return viewValue;
				};
				ctrl.$parsers.unshift(validator);
				ctrl.$formatters.unshift(validator);
			}
		}
	});
	/**
	 * Validate Equal
	 */
	app.directive('validateEqual', ['$parse', function($parse){
		return {
			restrict: 'A',
			require: 'ngModel',
			link(scope, element, attr, ctrl){
				const eq = $parse(attr['validateEqual']),
					validator = function(viewValue){
						// noinspection EqualityComparisonWithCoercionJS
						if(eq(scope) != viewValue){
							ctrl.$setValidity('validateEqual', false);
							element[0].setCustomValidity(" ");
							return undefined;
						}
						ctrl.$setValidity('validateEqual', true);
						element[0].setCustomValidity("");
						return viewValue;
					};
				ctrl.$parsers.unshift(validator);
				ctrl.$formatters.unshift(validator);
				scope.$watch(
					function(){
						return eq(scope);
					},
					function(){
						ctrl.$setViewValue(ctrl.$viewValue);
					}
				);
			}
		}
	}]);
}();
