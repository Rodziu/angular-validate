# AngularJS validation plugin for Twitter Bootstrap

AngularJS form validation integrated with [Twitter Bootstrap](https://getbootstrap.com/docs/3.3/).

See more at [Live Demo](https://mateuszrohde.pl/repository/angular-validate/demo/index.html)

## Prerequisites

- AngularJS,
- Twitter Bootstrap.

## Installation

```
yarn add angularjs-bootstrap-validate
yarn install
```