/*
 * AngularJS validation plugin for Twitter Bootstrap
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */

const app = angular.module('exampleApp', ['validate']);
app.controller('exampleCtrl', ['$scope', 'Validator', function($scope, Validator){
	$scope.submitJS = function(){
		console.log(Validator.validateForm("test"));
	};
	$scope.validateSub = function(){
		console.log(Validator.validateForm("subForm"));
	};
	$scope.reset = function(){
		Validator.resetValidator("test");
	};
	$scope.submit = function(){
		console.log(true);
	};
	$scope.email = '';
	$scope.isDirtyForm = function(name){
		return Validator.isDirtyForm(name);
	}
}]);
