/*
 * Angular extended select element plugin for AngularJS.
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
const pkg = require('./package'),
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify-es').default,
	sourcemaps = require('gulp-sourcemaps');


gulp.task('js', function(){
	return gulp.src(['src/js/**/*.js'])
		.pipe(concat(pkg.name + '.js'))
		.pipe(gulp.dest('dist/js'))
		.pipe(rename(pkg.name + '.min.js'))
		.pipe(sourcemaps.init())
		.pipe(uglify({
			output: {
				preamble: '/*! ' + pkg.description + ' v.' + pkg.version +
					' | (c) 2016-' + (new Date()).getFullYear() + ' ' + pkg.author + ' */'
			}
		}))
		.pipe(sourcemaps.write('./', {includeContent: false}))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('default', ['js']);